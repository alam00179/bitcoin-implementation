 ########::'#######::'##::: ##:'########:::::'###:::::'######:::'######::
 ##.....::'##.... ##: ###:: ##: ##.... ##:::'## ##:::'##... ##:'##... ##:
 ##::::::: ##:::: ##: ####: ##: ##:::: ##::'##:. ##:: ##:::..:: ##:::..::
 ######::: ##:::: ##: ## ## ##: ########::'##:::. ##:. ######::. ######::
 ##...:::: ##:::: ##: ##. ####: ##.....::: #########::..... ##::..... ##:
 ##::::::: ##:::: ##: ##:. ###: ##:::::::: ##.... ##:'##::: ##:'##::: ##:
 ########:. #######:: ##::. ##: ##:::::::: ##:::: ##:. ######::. ######::
#.......:::.......:::..::::..::..:::::::::..:::::..:::......::::......:::
# EONPASS protocol - transaction class
#    MIT License - Cryptomice 2018

import os
import hashlib
import configparser
from ecdsa import VerifyingKey, SigningKey, SECP256k1
from eonpass.bitcoin_rpc_class import RPCHost
from eonpass.poc import Poc
import time

class Transaction:

    EONclass_ = "transaction"
    EONversion_ = "0.0.1"

    def __init__(self, configFile):
        self.EONclass = ""
        self.EONversion = ""
        self.pocHash = ""
        self.chain = ""
        self.utxo = ""
        self.vout = ""
        self.transaction = ""
        self.signer = ""
        self.signature = ""
        self.configFile = configFile

        self.config = configparser.RawConfigParser()
        self.config.read(configFile)
        self.identity = self.config.get('EONPASS', 'identity')
        self.doc_path = self.config.get('EONPASS', 'documents')
        self.key_path = self.config.get('EONPASS', 'keys')

        os.makedirs(self.doc_path, exist_ok=True)
        os.makedirs(self.key_path, exist_ok=True)

    def print(self):
        print("--- Transaction - "+ self.hash())
        print(self.message(True))
        print("----------------------------------------")

    def sign(self):
        privKey = self.key_path+"/"+self.signer+"_private.pem"
        sk = SigningKey.from_pem(open(privKey).read())
        message = self.message(False)
        signature = sk.sign(message.encode('utf_8')).hex()
        return signature

    def checkSig(self):
        pubKey = self.key_path+"/"+self.signer+"_public.pem"
        vk = VerifyingKey.from_pem(open(pubKey).read())
        message = self.message(False)
        sig = bytes.fromhex(self.signature)
        try:
            res = vk.verify(sig, message.encode('utf_8'))
        except:
            print(self.filename+" has bad signature!")
            res = False
        return res

    def message(self, signature):
        message = "EONPASS Protocol: "+self.EONclass+": "+self.EONversion+"\n"
        message += "Poc: "+str(self.pocHash)+"\n"
        message += "SpentUtxo: "+str(self.chain)+":"+str(self.utxo)+":"+str(self.vout)+"\n"
        if (signature):
            message += "Signature: "+self.signer+":"+self.signature
        return message

    def hash(self):
        hash = hashlib.sha256(self.message(True).encode('utf-8')).hexdigest()
        return hash

    def load(self, filename):
        self.filename = filename
        file = open(self.doc_path+"/"+self.filename,"r",encoding='utf-8')
        for line in file:
            element = line.split(':')
            if element[0]=='EONPASS Protocol':
                self.EONclass = element[1].strip()
                self.EONversion = element[2].strip()
            elif element[0]=="Poc":
                self.pocHash = element[1].strip()
            elif element[0]=="SpentUtxo":
                self.chain = element[1].strip()
                self.utxo = element[2].strip()
                self.vout = element[3].strip()
            elif element[0]=="Signature":
                self.signer = element[1].strip()
                self.signature = element[2].strip()
            elif element[0]=="Transaction":
                self.transaction = element[1].strip()
        file.close()
        # Check file
        if (self.EONclass != self.EONclass_):
            print(self.filename+" has class "+self.EONclass+" but "+self.EONclass_+" is needed!")
            return None
        if (self.EONversion != self.EONversion_):
            print(self.filename+" has version "+self.EONversion+" but "+self.EONversion_+" is needed!")
            return None
        # Check chain
        # Check utxo
        # Check signature
        if self.checkSig():
            return self
        else:
            return None

    def create(self, poc):
        self.EONclass = self.EONclass_
        self.EONversion = self.EONversion_
        spoc = Poc(self.configFile).load(poc)
        if (spoc==None):
            print("POC "+spoc+" failed to load")
            return None
        self.pocHash = spoc.hash()
        self.chain = spoc.chain
        self.utxo = spoc.utxo
        self.vout = spoc.vout
        self.signer = self.identity
        self.signature = self.sign()
        # Check if unspent
        # Create string
        message = self.message(True);
        # Calculate hash
        hash = str(self.hash());
        # Save to file
        self.filename = hash+".transaction"
        file = open(self.doc_path+"/"+self.filename,"w+")
        file.write(message)
        file.close()
        return self;

    def waitUntilSpent(self, blocks):
        if (self.chain=='BTC'):
            rpcHost = self.config.get('BTC', 'host')
            rpcPort = self.config.get('BTC', 'port')
            rpcUser = self.config.get('BTC', 'username')
            rpcPassword = self.config.get('BTC', 'password')
            serverURL = 'http://' + rpcUser + ':' + rpcPassword + '@'+rpcHost+':' + str(rpcPort)
        elif (self.chain=='LTC'):
            rpcHost = self.config.get('LTC', 'host')
            rpcPort = self.config.get('LTC', 'port')
            rpcUser = self.config.get('LTC', 'username')
            rpcPassword = self.config.get('LTC', 'password')
            serverURL = 'http://' + rpcUser + ':' + rpcPassword + '@'+rpcHost+':' + str(rpcPort)
        else :
            print("ERROR: unknow chain ")
            return None
        host = RPCHost(serverURL)

        while (True):
            detailed_utxo = host.call('getrawtransaction',self.transaction, True)
            if ('confirmations' in detailed_utxo):
                if (detailed_utxo['confirmations'] >= blocks):
                    break
            else :
                print(".")
                time.sleep(10)
        print (self.transaction+" confirmed on "+self.chain+"!")
        return True

    def spend(self):
        if (self.chain=='BTC'):
            rpcHost = self.config.get('BTC', 'host')
            rpcPort = self.config.get('BTC', 'port')
            rpcUser = self.config.get('BTC', 'username')
            rpcPassword = self.config.get('BTC', 'password')
            change =  self.config.get('BTC', 'address')
            serverURL = 'http://' + rpcUser + ':' + rpcPassword + '@'+rpcHost+':' + str(rpcPort)
        elif (self.chain=='LTC'):
            rpcHost = self.config.get('LTC', 'host')
            rpcPort = self.config.get('LTC', 'port')
            rpcUser = self.config.get('LTC', 'username')
            rpcPassword = self.config.get('LTC', 'password')
            change =  self.config.get('LTC', 'address')
            serverURL = 'http://' + rpcUser + ':' + rpcPassword + '@'+rpcHost+':' + str(rpcPort)
        else :
            print("ERROR: unknow chain ")
            return
        host = RPCHost(serverURL)
        detailed_utxo = host.call('getrawtransaction',self.utxo, True)
        value = detailed_utxo['vout'][int(self.vout)]['value']
        print ("Transaction "+self.utxo+":"+self.vout+" value "+str(value))
        #fees = host.call('estimatesmartfee', 6)
        #fee = (120*fees['feerate'])/1000
        # in case of testnet
        fee=0
        if (fee < 0.00001000):
            fee = 0.00001000
        amount = value - fee
        amount = round(amount*100000000)/100000000
        if (amount<0):
            amount = 0
        #print ("After removing remain "+str(amount)+" with fees "+str(fee))
        raw_transaction = host.call('createrawtransaction', [{"txid":self.utxo,"vout":int(self.vout)}], {"data":self.pocHash, change:amount})
        signed_transaction = host.call('signrawtransaction', raw_transaction)
        #print (host.call('decoderawtransaction', signed_transaction['hex']))
        transaction = host.call('sendrawtransaction', signed_transaction['hex'])
        print ("Spent on "+self.chain+" with transaction "+transaction)
        self.transaction = transaction
        file = open(self.doc_path+"/"+self.filename,"a")
        file.write("\nTransaction: "+self.transaction)
        file.close()
