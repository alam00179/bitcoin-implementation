 ########::'#######::'##::: ##:'########:::::'###:::::'######:::'######::
 ##.....::'##.... ##: ###:: ##: ##.... ##:::'## ##:::'##... ##:'##... ##:
 ##::::::: ##:::: ##: ####: ##: ##:::: ##::'##:. ##:: ##:::..:: ##:::..::
 ######::: ##:::: ##: ## ## ##: ########::'##:::. ##:. ######::. ######::
 ##...:::: ##:::: ##: ##. ####: ##.....::: #########::..... ##::..... ##:
 ##::::::: ##:::: ##: ##:. ###: ##:::::::: ##.... ##:'##::: ##:'##::: ##:
 ########:. #######:: ##::. ##: ##:::::::: ##:::: ##:. ######::. ######::
#.......:::.......:::..::::..::..:::::::::..:::::..:::......::::......:::
# EONPASS protocol - contract class
#    MIT License - Cryptomice 2018

import os
import hashlib
import configparser
from ecdsa import VerifyingKey, SigningKey, SECP256k1
from eonpass.object import Object
from eonpass.utxo import Utxo

class Contract:

    EONclass_ = "contract"
    EONversion_ = "0.0.1"

    def __init__(self, configFile):
        self.EONclass = ""
        self.EONversion = ""
        self.filename = ""
        self.generator = ""
        self.objectList = []
        self.signer = ""
        self.signature = ""

        config = configparser.RawConfigParser()
        config.read(configFile)
        self.identity = config.get('EONPASS', 'identity')
        self.doc_path = config.get('EONPASS', 'documents')
        self.key_path = config.get('EONPASS', 'keys')
        self.configFile = configFile

        os.makedirs(self.doc_path, exist_ok=True)
        os.makedirs(self.key_path, exist_ok=True)

    def print(self):
        print("--- Contract - "+ self.hash())
        print(self.message(True))
        print("----------------------------------------")

    def sign(self):
        privKey = self.key_path+"/"+self.signer+"_private.pem"
        sk = SigningKey.from_pem(open(privKey).read())
        message = self.message(False)
        signature = sk.sign(message.encode('utf_8')).hex()
        return signature

    def checkSig(self):
        pubKey = self.key_path+"/"+self.signer+"_public.pem"
        vk = VerifyingKey.from_pem(open(pubKey).read())
        message = self.message(False)
        sig = bytes.fromhex(self.signature)
        try:
            res = vk.verify(sig, message.encode('utf_8'))
        except:
            print(self.filename+" has bad signature!")
            res = False
        return res

    def message(self, signature):
        message = "EONPASS Protocol: "+self.EONclass+": "+self.EONversion+"\n"
        message += "Generator: "+self.generator+"\n"
        for object in self.objectList:
            message += "Object: "+object+"\n"
        if (signature):
            message += "Signature: "+self.signer+":"+self.signature
        return message;

    def hash(self):
        hash = hashlib.sha256(self.message(True).encode('utf-8')).hexdigest()
        return hash

    def load(self, filename):
        self.filename = filename
        file = open(self.doc_path+"/"+self.filename,"r",encoding='utf-8')
        for line in file:
            element = line.split(':')
            if element[0]=='EONPASS Protocol':
                self.EONclass = element[1].strip()
                self.EONversion = element[2].strip()
            elif element[0]=="Generator":
                self.generator = str(element[1].strip())+":"+str(element[2].strip())+":"+str(element[3].strip())
            elif element[0]=="Object":
                self.objectList.append(element[1].strip())
            elif element[0]=="Signature":
                self.signer = element[1].strip()
                self.signature = element[2].strip()
        file.close()
        # Check file
        if (self.EONclass != self.EONclass_):
            print(self.filename+" has class "+self.EONclass+" but "+self.EONclass_+" is needed!")
            return None
        if (self.EONversion != self.EONversion_):
            print(self.filename+" has version "+self.EONversion+" but "+self.EONversion_+" is needed!")
            return None
        # Check generator ?
        # Check objects ?
        # Check signature
        if self.checkSig():
            return self
        else:
            return None

    def create(self, generator, objects):
        self.EONclass = self.EONclass_
        self.EONversion = self.EONversion_
        # Load anch check generator
        gen = Utxo(self.configFile).load(generator)
        if (gen==None):
            print("Generator "+generator+" failed to load")
            return None
        self.generator = gen.chain+":"+gen.utxo +":"+gen.vout
        # Load and check objects
        for object in objects:
            obj = Object(self.configFile).load(object)
            if (obj==None):
                print("Object "+object+" failed to load")
                return None
            self.objectList.append(str(obj.hash()))
        self.signer = self.identity
        self.signature = self.sign()
        # Create string
        message = self.message(True)
        # Calculate hash
        hash = str(self.hash());
        # Save to file
        self.filename = hash+".contract"
        file = open(self.doc_path+"/"+self.filename,"w+")
        file.write(message)
        file.close()
        return self;
