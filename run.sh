#!/bin/sh

# cleanup
rm documents/brand/*
rm documents/brand/incoming/*
rm documents/production/*
rm documents/production/incoming/*
rm documents/logistic/*
rm documents/logistic/incoming/*

# launch scripts
python3 brand.py &
python3 production.py &
python3 logistic.py &
